﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChartWindow;
using ChartWindowTest.Fakes;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace ChartWindowTest
{


    public class Mother
    {
        protected List<Point2> trianglePoints;
        protected List<Point2> anotherTrianglePoints;
        protected List<Point2> linePoints;

        protected RenderType inMainChartTriangles;
        protected RenderType inMainChartLines;

        public Mother()
        {
            this.trianglePoints = new List<Point2>();
            trianglePoints.Add(new Point2(0, 0));
            trianglePoints.Add(new Point2(100, 100));
            trianglePoints.Add(new Point2(100, 0));

            this.anotherTrianglePoints = new List<Point2>();
            anotherTrianglePoints.Add(new Point2(50, 20));
            anotherTrianglePoints.Add(new Point2(100, 200));
            anotherTrianglePoints.Add(new Point2(100, 100));

            this.linePoints = new List<Point2>();
            linePoints.Add(new Point2(0, 0));
            linePoints.Add(new Point2(100, 100));

            Color4 color = new Color4((float)1.0, (float)0.2, (float)0.3, (float)0.8);
            this.inMainChartTriangles = new RenderType(color, BeginMode.Triangles, (float)1.0);
            this.inMainChartLines = new RenderType(color, BeginMode.Lines, (float)1.0);
        }

        public FakeRenderBlock getRegularBlockTriangle(RenderType type)
        {
            return new FakeRenderBlock(type, this.trianglePoints);
        }

        public FakeRenderBlock getRegularBlockTriangle2(RenderType type)
        {
            return new FakeRenderBlock(type, this.anotherTrianglePoints);
        }

        public FakeRenderBlock getRegularBlockLine(RenderType type)
        {
            return new FakeRenderBlock(type, this.linePoints);
        }
    }
}

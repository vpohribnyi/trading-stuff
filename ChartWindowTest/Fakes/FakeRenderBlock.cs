﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChartWindow;

namespace ChartWindowTest.Fakes
{
    public class FakeRenderBlock : RenderBlock
    {
        public FakeRenderBlock (RenderType type, List<Point2> coordinates, List<RenderBlock> contents = null)
            :base(type, coordinates, contents = null)
        {
            
        }

        public List<Point2> getCoordinates()
        {
            return this.coordinates;
        }
    }
}

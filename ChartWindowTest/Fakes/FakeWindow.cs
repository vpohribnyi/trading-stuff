﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChartWindow;

namespace ChartWindowTest.Fakes
{
    public class FakeWindow : Window
    {
        public List<RenderBlock> getList()
        {
            return this.renderData;
        }
    }
}

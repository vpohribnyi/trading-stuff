﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using ChartWindow;
using ChartWindowTest.Fakes;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ChartWindowTest.WindowRendering
{
    [TestClass]
    public class Trivial :Mother
    {
        protected FakeWindow windowObject = new FakeWindow();

        [TestMethod]
        public void render_block_adds_to_list_after_calling_add_method()
        {
            windowObject.addRenderBlock(this.getRegularBlockTriangle(this.inMainChartTriangles));
        
            Assert.AreEqual(1, windowObject.getList().Count);
        }

        [TestMethod]
        public void gives_all_the_blocks_types_1_block_1_type()
        {
            RenderType expectedType = this.inMainChartTriangles;

            windowObject.addRenderBlock(this.getRegularBlockTriangle(this.inMainChartTriangles));

            Assert.AreSame(expectedType, windowObject.getTypes()[0]);
        }
        
        [TestMethod]
        public void gives_all_the_blocks_types_2_blocks_1_type()
        {
            RenderType expectedType = this.inMainChartTriangles;

            windowObject.addRenderBlock(this.getRegularBlockTriangle(this.inMainChartTriangles));
            windowObject.addRenderBlock(this.getRegularBlockTriangle2(this.inMainChartTriangles));

            Assert.AreSame(expectedType, windowObject.getTypes()[0]);
            Assert.AreEqual(1, windowObject.getTypes().Count);
        }

        [TestMethod]
        public void gives_all_the_blocks_types_2_blocks_2_types()
        {
            RenderType expectedType1 = this.inMainChartTriangles;
            RenderType expectedType2 = this.inMainChartLines;

            windowObject.addRenderBlock(this.getRegularBlockTriangle(this.inMainChartTriangles));
            windowObject.addRenderBlock(this.getRegularBlockTriangle(this.inMainChartLines));

            Assert.AreSame(expectedType1, windowObject.getTypes()[0]);
            Assert.AreSame(expectedType2, windowObject.getTypes()[1]);
            Assert.AreEqual(2, windowObject.getTypes().Count);
        }

        [TestMethod]
        public void gives_all_the_blocks_types_3_blocks_2_types()
        {
            RenderType expectedType1 = this.inMainChartTriangles;
            RenderType expectedType2 = this.inMainChartLines;

            windowObject.addRenderBlock(this.getRegularBlockTriangle(this.inMainChartTriangles));
            windowObject.addRenderBlock(this.getRegularBlockTriangle2(this.inMainChartTriangles));
            windowObject.addRenderBlock(this.getRegularBlockTriangle(this.inMainChartLines));

            Assert.AreSame(expectedType1, windowObject.getTypes()[0]);
            Assert.AreSame(expectedType2, windowObject.getTypes()[1]);
            Assert.AreEqual(2, windowObject.getTypes().Count);
        }

        [TestMethod]
        public void gives_coordinates_1_block_1_type()
        {
            FakeRenderBlock block = this.getRegularBlockTriangle(this.inMainChartTriangles);
            List<Point2> expectedPoints = block.getCoordinates();

            windowObject.addRenderBlock(block);

            List<Point2> currentPoints = windowObject.getRenderDataByType(this.inMainChartTriangles);
            Assert.IsTrue(expectedPoints.SequenceEqual(currentPoints));
        }

        [TestMethod]
        public void gives_coordinates_2_blocks_1_type()
        {
            FakeRenderBlock block1 = this.getRegularBlockTriangle(this.inMainChartTriangles);
            FakeRenderBlock block2 = this.getRegularBlockTriangle2(this.inMainChartTriangles);
            List<Point2> expectedPoints = new List<Point2>(block1.getCoordinates());
            expectedPoints.AddRange(block2.getCoordinates());

            windowObject.addRenderBlock(block1);
            windowObject.addRenderBlock(block2);

            List<Point2> currentPoints = windowObject.getRenderDataByType(this.inMainChartTriangles);
            Assert.IsTrue(expectedPoints.SequenceEqual(currentPoints));
        }

        [TestMethod]
        public void gives_coordinates_2_blocks_2_types()
        {
            FakeRenderBlock block1 = this.getRegularBlockTriangle(this.inMainChartTriangles);
            FakeRenderBlock block2 = this.getRegularBlockTriangle2(this.inMainChartTriangles);
            FakeRenderBlock block3 = this.getRegularBlockLine(this.inMainChartLines);
            List<Point2> expectedPoints = new List<Point2>(block3.getCoordinates());

            windowObject.addRenderBlock(block1);
            windowObject.addRenderBlock(block2);
            windowObject.addRenderBlock(block3);

            List<Point2> currentPoints = windowObject.getRenderDataByType(this.inMainChartLines);
            Assert.IsTrue(expectedPoints.SequenceEqual(currentPoints));
        }
    }
}

﻿using System;
using System.IO;
using ChartWindow.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ChartWindowTest.RenderBlockTest.InputValidation
{
    [TestClass]
    public class CoordinatesCountMatchesBeginMode
    {
        [TestMethod]
        [ExpectedException(typeof(InvalidInputException))]
        public void triangle_requested_2_points()
        {
            throw new NotImplementedException();
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidInputException))]
        public void triangle_requested_4_points()
        {
            throw new NotImplementedException();
        }

        [TestMethod]
        public void triangle_requested_3_points()
        {
            throw new NotImplementedException();
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidInputException))]
        public void line_requested_1_point()
        {
            throw new NotImplementedException();
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidInputException))]
        public void line_requested_5_points()
        {
            throw new NotImplementedException();
        }

        [TestMethod]
        public void line_requested_2_points()
        {
            throw new NotImplementedException();
        }

        [TestMethod]
        public void line_requested_4_points()
        {
            throw new NotImplementedException();
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidInputException))]
        public void quad_requested_1_point()
        {
            throw new NotImplementedException();
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidInputException))]
        public void quad_requested_5_points()
        {
            throw new NotImplementedException();
        }

        [TestMethod]
        public void quad_requested_4_points()
        {
            throw new NotImplementedException();
        }

        [TestMethod]
        public void quad_requested_8_points()
        {
            throw new NotImplementedException();
        }
    }
}

using System.Collections.Generic;

namespace ChartWindow
{
    public class Window
    {
        //Make this class in such a way that it orders all the 
        //data by render types, so that we can easily get 
        //both all the render types and render objects by type
        protected List<RenderBlock> renderData;
        protected List<RenderType> renderTypes; 

        public Window()
        {
            this.renderData = new List<RenderBlock>();
            this.renderTypes = new List<RenderType>();
        }

        public void clearLists()
        {
            renderTypes.Clear();
            renderData.Clear();
        }

        public void addRenderBlock(RenderBlock block)
        {
            if (!renderTypes.Contains(block.getType()))
                this.renderTypes.Add(block.getType());
            this.renderData.Add(block);
        }

        public List<Point2> getRenderDataByType(RenderType type)
        {
            
            List<Point2> result = new List<Point2>();
            foreach (RenderBlock block in this.renderData)
            {
                result.AddRange(block.getRenderDataByType(type));
            }

            return result;
        }

        public List<RenderType> getTypes()
        {
            return this.renderTypes;
        }
    }
}
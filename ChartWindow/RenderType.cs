using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace ChartWindow
{
    /// <summary>
    /// Represents different preferences for rendering UI, charts whatever
    /// </summary>
    public class RenderType
    {
        protected Color4 color;
        protected BeginMode beginMode;
        protected float moveRatio;      //Distance mouse is moved by distance graphics has been moved
        
        public RenderType(Color4 color, BeginMode beginMode, float moveRatio)
        {
            this.color = color;
            this.beginMode = beginMode;
            this.moveRatio = moveRatio;
        }
    }
}
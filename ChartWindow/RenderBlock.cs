using System.Collections.Generic;

namespace ChartWindow
{
    public class RenderBlock
    {
        protected RenderType type;
        protected List<Point2> coordinates;
        protected List<RenderBlock> contents;
        
        public RenderBlock(RenderType type, List<Point2> coordinates, List<RenderBlock> contents = null)
        {
            this.type = type;
            this.coordinates = coordinates;
            this.contents = (contents != null) && contents.GetType() == typeof(List<Point2>) ? 
                contents : new List<RenderBlock>();
        }

        public List<Point2> getRenderDataByType(RenderType type)
        {
            List<Point2> result;
            if (this.type == type)
                result = coordinates;
            else
                result = new List<Point2>();
            foreach (RenderBlock block in contents)
            {
                result.AddRange(block.getRenderDataByType(type));
            }
            return result;
        }

        public RenderType getType()
        {
            return this.type;
        }
    }
}